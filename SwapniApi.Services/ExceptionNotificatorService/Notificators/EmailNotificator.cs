﻿using SwapniApi.Services.EmailService;
using SwapniApi.Services.ExceptionNotificatorService.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Services.ExceptionNotificatorService.Notificators
{
    /// <summary>
    /// Нотификатор, отправляющий сообщения на почту
    /// </summary>
    public class EmailNotificator : IExceptionNotificator
    {
        private readonly SmptEmailService emailService;
        private readonly string email;

        public EmailNotificator(string email)
        {
            this.emailService = new SmptEmailService();
            this.email = email;
        }

        public void Notify(string message, Exception exception)
        {
            emailService.SendEmail(email, "Сообщение об ошибке", $"Ошибка: {message}\nИсключение: {exception.Message}");
        }

        public override string ToString()
        {
            return "Email " + email;
        }
    }
}
