﻿using SwapniApi.Services.ExceptionNotificatorService.Abstract;
using SwapniApi.Services.VkService;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Services.ExceptionNotificatorService.Notificators
{
    /// <summary>
    /// Нотификатор отправки сообщений в беседы ВК
    /// </summary>
    public class VkNotificator : IExceptionNotificator
    {
        private readonly int chatId;
        private VkGroupService api;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token">Токен сообщества</param>
        /// <param name="client">Клиент сообщества</param>
        /// <param name="chatId">Идентификатор чата (см. VkService)</param>
        public VkNotificator(string token, string client, int chatId)
        {
            api = new VkGroupService(token, client);
            this.chatId = chatId;
        }

        public void Notify(string message, Exception exception)
        {
            api.SendMessageToChat($"Ошибка: {message}\nИсключение: {exception.Message}", chatId);
        }

        public override string ToString()
        {
            return $"Vk notificator: chat_id: {chatId}";
        }
    }
}
