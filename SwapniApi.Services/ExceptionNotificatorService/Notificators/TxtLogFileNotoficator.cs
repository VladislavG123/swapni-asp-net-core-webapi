﻿using SwapniApi.Services.ExceptionNotificatorService.Abstract;
using SwapniApi.Services.LogService;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Services.ExceptionNotificatorService.Notificators
{
    public class TxtLogFileNotoficator : IExceptionNotificator
    {
        private TxtFileLogService service;

        public TxtLogFileNotoficator()
        {
            service = new TxtFileLogService();
        }


        public void Notify(string message, Exception exception)
        {
            service.Log(new DTOs.Services.LogServiceDTO
            {
                Executor = "Server",
                Type = "Exception",
                Details = $"Message: {message}. Exception: {exception.Message}"
            });
        }

        public override string ToString()
        {
            return "TxtFileLogger";
        }
    }
}
