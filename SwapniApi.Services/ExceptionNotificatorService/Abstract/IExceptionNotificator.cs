﻿using SwapniApi.Patterns.Observer;
using System;

namespace SwapniApi.Services.ExceptionNotificatorService.Abstract
{
    /// <summary>
    /// Интерфейс сервиса уведомления об ошибке 
    /// </summary>
    public interface IExceptionNotificator: IObserver
    {
        /// <summary>
        /// Уведомить 
        /// </summary>
        /// <param name="argument"></param>
        void Notify(string message, Exception exception);
    }
}
