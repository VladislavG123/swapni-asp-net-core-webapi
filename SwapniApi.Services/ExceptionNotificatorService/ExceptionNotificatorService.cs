﻿using SwapniApi.Services.ExceptionNotificatorService.Abstract;
using SwapniApi.Services.ExceptionNotificatorService.Notificators;
using System;
using System.Collections.Generic;

namespace SwapniApi.Services.ExceptionNotificatorService
{
    /// <summary>
    /// Сервис отправки уведомлений об ошибки
    /// </summary>
    public class ExceptionNotificatorService : Patterns.Observer.IObservable<IExceptionNotificator>
    {
        private List<IExceptionNotificator> observers = new List<IExceptionNotificator>()
        {
            new TxtLogFileNotoficator()
        };

        /// <summary>
        /// Получить список обозревателей 
        /// </summary>
        /// <returns></returns>
        public List<string> GetObservers()
        {
            var result = new List<string>();
            
            int index = 0;
            foreach (var observer in observers)
            {
                result.Add($"{index}) " + observer.ToString());
                index++;
            }

            return result;
        }

        /// <summary>
        /// Добавить новый обозреватель
        /// </summary>
        /// <param name="observer"></param>
        public void AddObserver(IExceptionNotificator observer)
        {
            observers.Add(observer);
        }

        /// <summary>
        /// Оповестить всех обозревателей об ошибке
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void NotifyObservers(string message, Exception exception)
        {
            foreach (var observer in observers)
            {
                observer.Notify(message, exception);
            }
        }

        /// <summary>
        /// Удалить обозреватель по идентификатор
        /// </summary>
        /// <param name="id">идентификатор обозревателя</param>
        public void RemoveObserver(int id)
        {
            observers.RemoveAt(id);
        }

        /// <summary>
        /// Удалить обозреватель по объекту
        /// </summary>
        /// <param name="observer">обозреватель</param>
        public void RemoveObserver(IExceptionNotificator observer)
        {
            observers.Remove(observer);
        }

        
    }
}
