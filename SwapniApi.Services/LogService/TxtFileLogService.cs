﻿using SwapniApi.DTOs.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SwapniApi.Services.LogService
{
    public class TxtFileLogService : ILogService
    {
        public async void Log(LogServiceDTO data)
        {
            string dirrectory = @"Logs/";
            string fileName = $"{DateTime.Now.ToShortDateString()}-log.txt";
            if (!Directory.Exists(dirrectory))
            {
                Directory.CreateDirectory(dirrectory);
            }

            await File.AppendAllTextAsync(dirrectory + fileName, data.ToString());
        }
    }
}
