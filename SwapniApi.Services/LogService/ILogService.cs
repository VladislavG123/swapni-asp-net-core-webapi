﻿using SwapniApi.DTOs.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Services.LogService
{
    public interface ILogService
    {
        void Log(LogServiceDTO data);
    }
}
