﻿using SwapniApi.DTOs.Services.EmailService;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Services.EmailService.Abstract
{
    public interface IEmailService
    {
        EmailServiceResponseDTO SendEmail(string email, string body, string message);
    }
}
