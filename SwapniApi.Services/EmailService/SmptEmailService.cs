﻿using SwapniApi.DTOs.Services.EmailService;
using SwapniApi.Services.EmailService.Abstract;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace SwapniApi.Services.EmailService
{
    public class SmptEmailService : IEmailService
    {
        public EmailServiceResponseDTO SendEmail(string email, string subject, string body)
        {
            int code = 200;
            string response = "Mail has sended";

            try
            {
                //Порт SMTP - 25, 587 или 2525
                SmtpClient smtp = new SmtpClient("smtp.mail.ru", 25);
                smtp.Credentials = new System.Net.NetworkCredential("your email", "password");
                smtp.EnableSsl = true;

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("your email");
                mail.To.Add(new MailAddress(email));
                mail.Subject = "Swapni.su | " + subject;
                mail.Body = body + 
                            "\n\n\n" +
                            "__________________________\n" +
                            "swapni.su";

                smtp.Send(mail);
            }
            catch (Exception exception)
            {
                code = 500;
                response = "Неопозднанная ошибка сервиса. Ошибка: " + exception.Message;
            }

            return new EmailServiceResponseDTO { Code = code, Response = response };
        }
    }
}
