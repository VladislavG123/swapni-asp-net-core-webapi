﻿using System;
using System.Collections.Generic;
using System.Text;
using VkNet;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace SwapniApi.Services.VkService
{
    /// <summary>
    /// Сервис доступа к системе VK
    /// </summary>
    public class VkGroupService
    {
        private VkApi api;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token">Токен бота от сообщества</param>
        /// <param name="client">Клиент сообщества</param>
        public VkGroupService(string token, string client)
        {
            api = new VkNet.VkApi();
            api.Authorize(new ApiAuthParams { AccessToken = token, ClientSecret = client });
        }

        /// <summary>
        /// Отправка сообщений в беседу(не ЛС)
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="chatId">Идентификатор чата</param>
        public void SendMessageToChat(string message, int chatId)
        {
            var random = new Random();
            api.Messages.Send(new MessagesSendParams
            {
                RandomId = random.Next(),
                ChatId = chatId,
                Message = message
            });
        }
    }
}
