﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SwapniApi.DataAccess;
using System;
using SwapniApi.Options;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using SwapniApi.Models;
using SwapniApi.DTOs.Services.Authentication;
using SwapniApi.Exceptions.Services;

namespace SwapniApi.Services
{
    public class AuthenticationService
    {
        private readonly SwapniContext context;
        private readonly string jwtSecret;

        public AuthenticationService(SwapniContext context, IOptions<SecretOption> secretOptions)
        {
            this.context = context;
            this.jwtSecret = secretOptions.Value.JWTSecret;
        }

        /// <summary>
        /// Предоставление токена на основе существующего пользователя
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns>Jwt токен</returns>
        public async Task<string> Authenticate(string email, string password)
        {
            // Находим модель на основе предоставленных данных
            var user = await context.Users.FirstOrDefaultAsync(user => user.Email == email);

            // Если пользователь не найден, или же пароли не совпадают, вернуть false
            if (user is null || !new PasswordHashService().Validate(password, user.PasswordHash)) return null;

            // Генерируем Jwt токен на основе Email адреса
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(jwtSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, email),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(securityToken);
        }

        /// <summary>
        /// Предоставление токена и создание нового пользователя
        /// </summary>
        /// <param name="username"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<string> Registrate(string username, string email, string password)
        {
            // Пытаемся найти пользователя на основе введённых данных
            var user = await context.Users.FirstOrDefaultAsync(user => user.Email == email);

            // Если такой пользователь уже есть, вернуть null
            if (user != null) return null;

            // Добавляем нового пользователя в таблицу пользователей
            context.Users.Add(new Models.User
            {
                Email = email,
                Username = username,
                PasswordHash = new PasswordHashService().CreatePasswordHash(password)
            });

            // Сохраняем изменения
            await context.SaveChangesAsync();

            // Генерируем токен на основе почты
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(jwtSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, email),
                    new Claim(ClaimTypes.Role, Role.User)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(securityToken);

        }

        /// <summary>
        /// Расшифровка токена
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Email адрес пользователя</returns>
        public ClaimsDTO DecryptToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();

            var tokenS = handler.ReadToken(token) as JwtSecurityToken;

            var claims = tokenS.Claims as List<Claim>;

            return new ClaimsDTO
            {
                Email = claims[0].Value,
                Role = claims[1].Value
            };
        }

        /// <summary>
        /// Возвращает пользователя по его JWT токену
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<User> GetCurrentUserFromTokenAsync(string token)
        {
            var claims = DecryptToken(token);

            return await GetCurrentUserFromEmailAsync(claims.Email);
        }

        /// <summary>
        /// Возвращает пользователя по его email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<User> GetCurrentUserFromEmailAsync(string email)
        {
            var user = await context.Users.FirstOrDefaultAsync(x => x.Email == email);

            if (user is null)
            {
                throw new UserNotFoundException();
            }

            return user;
        }

    }
}
