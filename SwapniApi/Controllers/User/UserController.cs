﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Cryptography.Xml;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SwapniApi.DataAccess;
using SwapniApi.DTOs.User;
using SwapniApi.Services;
using SwapniApi.Services.ExceptionNotificatorService;

namespace SwapniApi.Controllers.User
{
    [Route("api/user/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly SwapniContext context;
        private readonly ExceptionNotificatorService exceptionNotificator;
        private readonly Services.AuthenticationService authenticationService;
        private readonly PasswordHashService passwordHashService;

        public UserController(SwapniContext context, ExceptionNotificatorService exceptionNotificator, 
                              Services.AuthenticationService authenticationService, PasswordHashService passwordHashService)
        {
            this.context = context;
            this.exceptionNotificator = exceptionNotificator;
            this.authenticationService = authenticationService;
            this.passwordHashService = passwordHashService;
        }


        /// <summary>
        /// Метод изменения пароля
        /// </summary>
        /// <param name="OldPassword"></param>
        /// <param name="NewPassword"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordDTO resetPasswordDTO)
        {
            try
            {
                var user = await authenticationService.GetCurrentUserFromEmailAsync(
                    ((User.Identities as List<ClaimsIdentity>)[0].Claims as List<Claim>)[0].Value);

                if (!passwordHashService.Validate(resetPasswordDTO.OldPassword, user.PasswordHash))
                {
                    return ValidationProblem("Passwords are not equals");
                }

                user.PasswordHash = passwordHashService.CreatePasswordHash(resetPasswordDTO.NewPassword);

                await context.SaveChangesAsync();
                
                return Ok();
            }
            catch (Exception exception)
            {
                exceptionNotificator.NotifyObservers("Ошибка изменения пароля у пользователя ", exception);
                return BadRequest(exception.Message);
            }
        }
    }
}
