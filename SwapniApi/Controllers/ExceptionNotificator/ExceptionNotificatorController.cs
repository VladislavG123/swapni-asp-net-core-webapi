﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SwapniApi.Models;
using SwapniApi.Services.ExceptionNotificatorService.Notificators;

namespace SwapniApi.Controllers.ExceptionNotificator
{
    /// <summary>
    /// Контроллер администрации системы информирования об ошибках
    /// </summary>
    [Route("api/dev/exceptions/[action]")]
    
    [ApiController]
    [Authorize(Roles = Role.Developer)]
    public class ExceptionNotificatorController : ControllerBase
    {
        private readonly Services.ExceptionNotificatorService.ExceptionNotificatorService notificator;

        public ExceptionNotificatorController(Services.ExceptionNotificatorService.ExceptionNotificatorService notificator)
        {
            this.notificator = notificator;
        }

        /// <summary>
        /// Добавить Email нотификатор
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [ActionName("addemail")]
        [HttpPost]
        public IActionResult AddEmailNotificator(string email)
        {
            notificator.AddObserver(new EmailNotificator(email));
            return Ok();
        }

        /// <summary>
        /// Добавить Vk нотификатор
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [ActionName("addvk")]
        [HttpPost]
        public IActionResult AddVkNotificator(string token, string client, int chatId)
        {
            notificator.AddObserver(new VkNotificator(token, client, chatId));
            return Ok();
        }

        /// <summary>
        /// Получить все текущие нотификаторы
        /// </summary>
        /// <returns></returns>
        [ActionName("getall")]
        [HttpGet]
        public IActionResult GetNotificators()
        {
            return Ok(notificator.GetObservers());
        }

        /// <summary>
        /// Тестирование выврда ошибки
        /// </summary>
        /// <returns></returns>
        [ActionName("try")]
        [HttpPatch]
        public IActionResult Try()
        {
            try
            {
                throw new System.Exception("Test error");
            }
            catch (System.Exception e)
            {
                notificator.NotifyObservers("Test error!", e);
            }

            return Ok();
        }

        /// <summary>
        /// Удалить нотификатор по идентификатору
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        [ActionName("remove")]
        [HttpPut]
        public IActionResult RemoveObserver(int index)
        {
            notificator.RemoveObserver(index);

            return Ok();
        }
    }
}
