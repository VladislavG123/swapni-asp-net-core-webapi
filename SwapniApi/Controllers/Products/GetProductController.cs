﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SwapniApi.DataAccess;
using SwapniApi.DTOs.Products;
using SwapniApi.Services.ExceptionNotificatorService;

namespace SwapniApi.Controllers.Products
{
    [Route("api/product/get/[Action]")]
    [ApiController]
    public class GetProductController : ControllerBase
    {
        private readonly SwapniContext context;
        private readonly ExceptionNotificatorService exceptionNotificator;

        public GetProductController(SwapniContext context, ExceptionNotificatorService exceptionNotificator)
        {
            this.context = context;
            this.exceptionNotificator = exceptionNotificator;
        }

        [HttpGet]
        [ActionName("one")]
        public async Task<IActionResult> GetConcrete(Guid id)
        {
            try
            {
                var product = (await(from item in context.Products.Take(1)
                                     join user in context.Users on item.OwnerId equals user.Id
                                     
                                     select new GetProductDTO
                                     {
                                         Id = item.Id,
                                         Coins = item.Coins,
                                         CreationDate = item.CreationDate,
                                         Description = item.Description,
                                         OwnerEmail = user.Email,
                                         Title = item.Title,
                                         PhotoUrls = (from photo in context.Photos
                                                      join productsPhotos in context.ProductsPhotos on item.Id equals productsPhotos.ProductId
                                                      where productsPhotos.ProductId == item.Id
                                                      select photo.Url).ToList()
                                     }).ToListAsync()).First();
                return Ok(product);
            }
            catch (Exception exception)
            {
                exceptionNotificator.NotifyObservers("Ошибка получения продуктa", exception);
                return Problem();
            }
        }
    }
}
