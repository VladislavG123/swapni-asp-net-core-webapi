﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SwapniApi.DataAccess;
using SwapniApi.DTOs.Products;
using SwapniApi.Services.ExceptionNotificatorService;

namespace SwapniApi.Controllers.Products
{

    /// <summary>
    /// Контроллер получения продуктов
    /// </summary>
    [Route("api/products/get/[Action]")]
    [ApiController]
    public class GetProductsController : ControllerBase
    {
        private readonly SwapniContext context;
        private readonly ExceptionNotificatorService exceptionNotificator;

        public GetProductsController(SwapniContext context, ExceptionNotificatorService exceptionNotificator)
        {
            this.context = context;
            this.exceptionNotificator = exceptionNotificator;
        }

        [HttpGet]
        [ActionName("all")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var products = await (from product in context.Products
                                      join user in context.Users on product.OwnerId equals user.Id
                                      select new GetProductsDTO
                                      {
                                          Id = product.Id,
                                          Price = product.Coins,
                                          DateTime = product.CreationDate,
                                          Title = product.Title,
                                          PhotoUrl = (from photo in context.Photos.Take(1)
                                                       join productsPhotos in context.ProductsPhotos on product.Id equals productsPhotos.ProductId
                                                       where productsPhotos.ProductId == product.Id
                                                       select photo.Url).ToList().First() ?? ""
                                      }).ToListAsync();
                return Ok(products);
            }
            catch (Exception exception)
            {
                exceptionNotificator.NotifyObservers("Ошибка получения продуктов", exception);
                return Problem();
            }
        }


        [HttpGet]
        [ActionName("all/owners")]
        public async Task<IActionResult> GetAllWithOwnerEmail(string email)
        {
            try
            {
                var products = await (from product in context.Products
                                      join user in context.Users on product.OwnerId equals user.Id
                                      where user.Email == email
                                      select new GetProductsDTO
                                      {
                                          Id = product.Id,
                                          Price = product.Coins,
                                          DateTime = product.CreationDate,
                                          Title = product.Title,
                                          PhotoUrl = (from photo in context.Photos.Take(1)
                                                      join productsPhotos in context.ProductsPhotos on product.Id equals productsPhotos.ProductId
                                                      where productsPhotos.ProductId == product.Id
                                                      select photo.Url).ToList().First() ?? ""
                                      }).ToListAsync();
                return Ok(products);
            }
            catch (Exception exception)
            {
                exceptionNotificator.NotifyObservers("Ошибка получения продуктов", exception);
                return Problem();
            }
        }


        [HttpGet]
        [ActionName("pagination")]
        public async Task<IActionResult> GetPagination(int count, int startIndex)
        {
            try
            {
                var products = await (from product in context.Products.Skip(startIndex).Take(count)
                                      join user in context.Users on product.OwnerId equals user.Id
                                      select new GetProductsDTO
                                      {
                                          Id = product.Id,
                                          Price = product.Coins,
                                          DateTime = product.CreationDate,
                                          Title = product.Title,
                                          PhotoUrl = (from photo in context.Photos.Take(1)
                                                      join productsPhotos in context.ProductsPhotos on product.Id equals productsPhotos.ProductId
                                                      where productsPhotos.ProductId == product.Id
                                                      select photo.Url).ToList().First() ?? ""
                                      }).ToListAsync();
                return Ok(products);
            }
            catch (Exception exception)
            {
                exceptionNotificator.NotifyObservers("Ошибка получения продуктов", exception);
                return Problem();
            }
        }

    }
}
