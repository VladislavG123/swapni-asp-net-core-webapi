﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SwapniApi.DataAccess;
using SwapniApi.DTOs.Products;
using SwapniApi.Models;
using SwapniApi.Services;
using SwapniApi.Services.ExceptionNotificatorService;

namespace SwapniApi.Controllers.Products
{
    [Route("api/products/add/[Action]")]
    [ApiController]
    public class AddProductController : ControllerBase
    {
        private readonly ExceptionNotificatorService exceptionNotificator;
        private readonly SwapniContext context;
        private readonly AuthenticationService authenticationService;

        public AddProductController(ExceptionNotificatorService exceptionNotificator,
                                    SwapniContext context, AuthenticationService authenticationService)
        {
            this.exceptionNotificator = exceptionNotificator;
            this.context = context;
            this.authenticationService = authenticationService;
        }

        [HttpPost]
        [ActionName("range")]
        [Authorize]
        public async Task<IActionResult> AddRange(AddRangeProductsDTO productDTOs)
        {
            try
            {
                // Получаем пользователя через JWT
                var user = await authenticationService
                               .GetCurrentUserFromEmailAsync(
                                    User.Identities.First().Claims.First().Value);

                // Создаем продукты на основе запроса и добавляем их в базу
                var products = CreateProducts(new Product
                {
                    Coins = productDTOs.Product.Coins,
                    Description = productDTOs.Product.Description,
                    Title = productDTOs.Product.Title,
                    OwnerId = user.Id
                }, productDTOs.Count);
                await context.Products.AddRangeAsync(products);

                // Аналогично с фотографиями
                var photos = CreatePhotos(productDTOs.Product.PhotoUrls);
                await context.Photos.AddRangeAsync(photos);

                // На основе продуктов и фотографий, создаем связь многие ко многим
                await context.ProductsPhotos.AddRangeAsync(CreateRelations(products, photos));

                // Фиксируем в базе
                await context.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                exceptionNotificator.NotifyObservers("Ошибка добавления AddRange AddProductController", exception);
                return Problem("Предмет(ы) не добавлены");
            }

            return Ok();
        }


        [HttpPost]
        [ActionName("one")]
        [Authorize]
        public async Task<IActionResult> AddOne(AddOneProductDTO productDTO)
        {
            return await AddRange(new AddRangeProductsDTO { Product = productDTO, Count = 1 });
        }

        private List<Product> CreateProducts(Product product, int count)
        {
            var result = new List<Product>();

            for (int i = 0; i < count; i++)
            {
                product.Id = Guid.NewGuid();
                
                result.Add(product.GetClone());
            }

            return result;
        }

        private List<Photo> CreatePhotos(List<string> urls)
        {
            var result = new List<Photo>();

            foreach (var url in urls)
            {
                result.Add(new Photo { Url = url });
            }

            return result;
        }

        private List<ProductsPhoto> CreateRelations(List<Product> products, List<Photo> photos)
        {
            var result = new List<ProductsPhoto>();

            foreach (var product in products)
            {
                foreach (var photo in photos)
                {
                    result.Add(new ProductsPhoto
                    {
                        ProductId = product.Id,
                        PhotoId = photo.Id
                    });
                }
            }

            return result;
        }


    }
}
