﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SwapniApi.DataAccess;
using SwapniApi.DTOs.Authentication;
using SwapniApi.Models;
using SwapniApi.Services;
using SwapniApi.Services.ExceptionNotificatorService;
using SwapniApi.Services.ExceptionNotificatorService.Abstract;
using SwapniApi.Services.LogService;

namespace SwapniApi.Controllers.Authentication
{
    /// <summary>
    /// Контроллер аутентификации через email
    /// </summary>
    [Route("api/auth/[action]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly SwapniContext context;
        private readonly AuthenticationService authenticationService;
        private readonly ExceptionNotificatorService exceptionNotificator;

        public AuthenticationController(SwapniContext context, AuthenticationService authenticationService, ExceptionNotificatorService exceptionNotificator)
        {
            this.context = context;
            this.authenticationService = authenticationService;
            this.exceptionNotificator = exceptionNotificator;
        }

        /// <summary>
        /// Регистрация нового пользователя
        /// </summary>
        /// <param name="authenticationData">Класс для аутентификации. Имеет свойства: Email, Username, Password</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("signup")]
        public async Task<IActionResult> SignUp(SignUpDataDTO authenticationData)
        {
            try
            {
                // Генерируем JWT токен
                var token = await authenticationService.Registrate(
                    authenticationData.Username,
                    authenticationData.Email,
                    authenticationData.Password);

                // В случае если произошла ошибка генерации, вернуть ошибку плохого запроса
                if (token is null)
                {
                    return BadRequest();
                }
                // Вернуть токен клиенту
                return Ok(new { token });
            }
            catch (Exception exception)
            {
                this.exceptionNotificator.NotifyObservers(
                    $"Ошибка регистрации пользователя: {authenticationData.Email}",
                    exception);
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Вход в аккаунт
        /// </summary>
        /// <param name="authenticationData">Класс для аутентификации. Имеет свойства: Email, Username, Password</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("signin")]
        public async Task<IActionResult> SignIn(SignInDataDTO authenticationData)
        {
            try
            {
                // Генерируем JWT токен
                var token = await authenticationService.Authenticate(authenticationData.Email,
                                                                     authenticationData.Password);

                // В случае если произошла ошибка генерации, вернуть ошибку плохого запроса
                if (token is null)
                {
                    return BadRequest();
                }
                // Вернуть токен клиенту
                return Ok(new { token });
            }
            catch (Exception exception)
            {
                this.exceptionNotificator.NotifyObservers(
                    $"Ошибка авторизации пользователя: {authenticationData.Email}",
                    exception);
                return StatusCode(500);
            }
        }


    }
}
