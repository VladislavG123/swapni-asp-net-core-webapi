Проект представляет собой шаблон API интернет магазина
В проекте есть:
1) Разделение решения по проектам
2) Entity Framework
3) Базовые модели 
4) Первичная настройка Cors
5) Swagger UI
6) Unit tests
7) Авторизация и аутентификация
8) Контроллеры для аутентификации, управления пользователем, создание и получение товаров
9) Сервис работы с ботом ВК
10) Сервис логгирования 
11) Сервис оповещения об исключениях на основе паттерна Observer

Код сопровожден комментариями во многих местах 