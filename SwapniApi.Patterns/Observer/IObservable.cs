﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Patterns.Observer
{
    /// <summary>
    /// Интерфейс реализации паттерна Наблюдатель
    /// </summary>
    /// <typeparam name="T">Абстрактный тип наблюдателя</typeparam>
    public interface IObservable<T> where T: IObserver
    {
        void AddObserver(T observer);
        void RemoveObserver(T observer);
        void NotifyObservers(string message, Exception exception);
    }
}
