﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SwapniApi.DataAccess.Migrations
{
    public partial class AddedRoleForUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdminLevel",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "Role",
                table: "Users",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Role",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "AdminLevel",
                table: "Users",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
