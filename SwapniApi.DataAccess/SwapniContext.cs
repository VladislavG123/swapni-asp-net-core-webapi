﻿using Microsoft.EntityFrameworkCore;
using SwapniApi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.DataAccess
{
    /// <summary>
    /// Класс, предоставляющий доступ к таблицам базы данных
    /// </summary>
    public class SwapniContext : DbContext
    {
        public SwapniContext(DbContextOptions options) : base(options)
        {
            Database.Migrate();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<ProductsPhoto> ProductsPhotos { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();
        }
    }
}
