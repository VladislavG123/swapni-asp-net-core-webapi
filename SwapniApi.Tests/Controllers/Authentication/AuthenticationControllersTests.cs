using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using SwapniApi.Controllers.Authentication;
using SwapniApi.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SwapniApi.Tests
{
    /// <summary>
    /// ����� ��� AuthenticationController
    /// </summary>
    public class AuthenticationControllersTests
    {
        private AuthenticationController controller;
        private MockGenerator mockGenerator = new MockGenerator();

        [SetUp]
        public void Setup()
        {
            controller = new AuthenticationController(mockGenerator.GetSwapniContext(),
                                                      mockGenerator.GetAuthenticationService(),
                                                      mockGenerator.GetExceptionNotificatorService());
        }

        /// <summary>
        /// ���������� ���� ��� ����������� 
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task RegistrationPositiveTest()
        {
            // Arrange
            var data = await controller.SignUp(new DTOs.Authentication.SignUpDataDTO()
            {
                Email = $"TestAuthController{DateTime.Now}@test.loc",
                Username = "Test",
                Password = "123123"
            });

            // Act
            data = data as ActionResult;

            // Assert
            var okObjectResult = data as OkObjectResult;
            Assert.IsNotNull(okObjectResult);
          
        }
    }
}