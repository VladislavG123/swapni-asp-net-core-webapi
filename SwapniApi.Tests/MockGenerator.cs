﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using SwapniApi.DataAccess;
using SwapniApi.Options;
using SwapniApi.Services;
using SwapniApi.Services.ExceptionNotificatorService;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Tests
{
    /// <summary>
    /// Класс, предоставляющий тестовые данных для сервисов, которые объявлены в Startup.cs
    /// </summary>
    public class MockGenerator
    {
        private string connectionString = "Server=localhost;Database=SwapniDbLocal;Trusted_Connection=True;";
        private string jwtSecret = "5fb19c55-4a1b-481a-af93-90a5411351fb";

        /// <summary>
        /// Парамметры базы данных
        /// </summary>
        /// <returns></returns>
        public DbContextOptions GetDbContextOptions()
        {
            return new DbContextOptionsBuilder<SwapniContext>()
                  .UseSqlServer(connectionString)
                  .Options;
        }

        /// <summary>
        /// Объект контекста базы данных
        /// </summary>
        /// <returns></returns>
        public SwapniContext GetSwapniContext()
        {
            return new SwapniContext(GetDbContextOptions());
        }

        /// <summary>
        /// Мок объект для ключа от JWT
        /// </summary>
        /// <returns></returns>
        public Mock<IOptions<SecretOption>> GetMockOfSecretOptions()
        {
            var mock = new Mock<IOptions<SecretOption>>();
            mock.Setup(x => x.Value).Returns(new SecretOption { JWTSecret = jwtSecret });
            return mock;
        }

        /// <summary>
        /// Получить объект сервиса аутентификации
        /// </summary>
        /// <returns></returns>
        public AuthenticationService GetAuthenticationService()
        {
            return new AuthenticationService(GetSwapniContext(), GetMockOfSecretOptions().Object);
        }

        /// <summary>
        /// Получить объект сервиса оповещения об исключениях
        /// </summary>
        /// <returns></returns>
        public ExceptionNotificatorService GetExceptionNotificatorService()
        {
            return new ExceptionNotificatorService();
        }
    }
}
