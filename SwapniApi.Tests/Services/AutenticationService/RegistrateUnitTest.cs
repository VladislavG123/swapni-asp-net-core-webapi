using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using SwapniApi.DataAccess;
using SwapniApi.Options;
using SwapniApi.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SwapniApi.Tests
{
    /// <summary>
    /// ����� ��� ������ ����������� ������� ��������������
    /// </summary>
    public class RegistrateUnitTest
    {
        private MockGenerator mockGenerator = new MockGenerator();
        private AuthenticationService authenticationService;

        [SetUp]
        public void Setup()
        {
            authenticationService = mockGenerator.GetAuthenticationService();
        }

        [Test]
        public async Task PositiveTest()
        {
            // Act
            var token = await authenticationService.Registrate("Test", $"TestAuthService{DateTime.Now}@test.loc", "213123");

            // Assert
            if (token is null)
            {
                Assert.Fail();
                return;
            }

            Assert.Pass();
        }
    }
}