﻿using NUnit.Framework;
using SwapniApi.Services.VkService;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Tests.Services.VkNetService
{
    /// <summary>
    /// Тест отправки сообщений через сервис VK
    /// </summary>
   public class SendMessageTests
    {
        private VkGroupService api;

        [SetUp]
        public void Setup()
        {
            // Ввести свои данные
            api = new VkGroupService("***", "***");
        }

        /// <summary>
        /// Отправка сообщения
        /// </summary>
        [Test]
        public void Test1()
        {
            api.SendMessageToChat("Test", 2);

            Assert.Pass();
        }
    }
}
