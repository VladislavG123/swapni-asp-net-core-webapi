﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using SwapniApi.Services.ExceptionNotificatorService;
using SwapniApi.Services.ExceptionNotificatorService.Notificators;

namespace SwapniApi.Tests.Services.ExceptionNotificatorService
{
    /// <summary>
    /// Тесты для сервиса уведомления об ошибке
    /// </summary>
    public class ExceptionNotificatorService
    {
        private SwapniApi.Services.ExceptionNotificatorService.ExceptionNotificatorService notificator;

        [SetUp]
        public void Setup()
        {
             notificator = new SwapniApi.Services.ExceptionNotificatorService.ExceptionNotificatorService();
        }

        [Test]
        public void AddObserver()
        {
            notificator.AddObserver(new EmailNotificator("gvo_step2018@mail.ru"));

            try
            {
                throw new Exception("Тестовая ошибка!");
            }
            catch (Exception e)
            {
                notificator.NotifyObservers("123", e);
            }

            Assert.Pass();
        }

        [Test]
        public void RemoveObserver()
        {
            notificator.AddObserver(new EmailNotificator("gvo_step2018@mail.ru"));

            var count = notificator.GetObservers().Count;
            
            notificator.RemoveObserver(0);

            if (notificator.GetObservers().Count != count - 1)
            {
                Assert.Fail();
            }

            Assert.Pass();
        }
    }
}
