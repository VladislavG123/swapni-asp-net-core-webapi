﻿using NUnit.Framework;
using SwapniApi.Services.LogService;
using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Tests.Services.LogService
{
    public class TxtFileLogService
    {
        private SwapniApi.Services.LogService.TxtFileLogService service;

        [SetUp]
        public void Setup()
        {
            service = new SwapniApi.Services.LogService.TxtFileLogService();
        }

        [Test]
        public void Test1()
        {
            service.Log(new DTOs.Services.LogServiceDTO { Details = "testtest", Executor = "UnitTest", Type = "Test" });

            Assert.Pass();
        }
    }
}
