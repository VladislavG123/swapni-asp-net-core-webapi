﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SwapniApi.DTOs.Products
{
    public class AddOneProductDTO
    {
        [MinLength(5)]
        [MaxLength(100)]
        public string Title { get; set; }
        [MinLength(5)]
        public string Description { get; set; }
        [Range(50, Int32.MaxValue)]
        public int Coins { get; set; }

        public List<string> PhotoUrls { get; set; }
    }
}
