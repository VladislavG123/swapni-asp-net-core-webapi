﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.DTOs.Products
{
    public class GetProductsDTO
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string City { get; set; }
        public DateTime DateTime { get; set; }
        public int Price { get; set; }
        public string PhotoUrl { get; set; }
    }
}
