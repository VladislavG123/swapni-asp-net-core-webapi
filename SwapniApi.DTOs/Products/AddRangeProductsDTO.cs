﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SwapniApi.DTOs.Products
{
    public class AddRangeProductsDTO
    {
        [Required]
        public AddOneProductDTO Product { get; set; }

        [Range(1, 10)]
        [Required]
        public int Count { get; set; }
    }
}
