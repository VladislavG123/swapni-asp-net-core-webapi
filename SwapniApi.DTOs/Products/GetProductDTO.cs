﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.DTOs.Products
{
    public class GetProductDTO
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Coins { get; set; }
        public DateTime CreationDate { get; set; }
        public string OwnerEmail { get; set; }

        public List<string> PhotoUrls { get; set; }
    }
}
