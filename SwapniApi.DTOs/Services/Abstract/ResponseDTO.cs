﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.DTOs.Services.Abstract
{
    public abstract class ResponseDTO
    {
        /// <summary>
        /// Текствый результат ответа
        /// </summary>
        public string Response { get; set; }

        /// <summary>
        /// Код ответа от сервиса
        /// 1xx - информационный ответ
        /// 2xx - ответ успеха
        /// 3xx - ответ незначительной ошибки
        /// 4xx - ответ ошибки со стороны клиента
        /// 5xx - ответ ошибки со стороны сервиса
        /// </summary>
        public int Code { get; set; }
    }
}
