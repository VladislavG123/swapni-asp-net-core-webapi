﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.DTOs.Services.Authentication
{
    /// <summary>
    /// Класс транспортировки данных для клеймов JTW токена
    /// </summary>
    public class ClaimsDTO
    {
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
