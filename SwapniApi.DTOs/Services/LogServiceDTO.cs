﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.DTOs.Services
{
    public class LogServiceDTO
    {
        public DateTime DateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// Сущность, которая производит логгирование (SERVER, USER, ADMIN...)
        /// </summary>
        public string Executor { get; set; }
        /// <summary>
        /// Тип или же причина вызова. Оплата, Ошибка, Регистрация
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Описание
        /// </summary>
        public string Details { get; set; }

        public override string ToString()
        {
            return $@"{DateTime.ToShortTimeString()}${Executor}${Type}${Details}|";
        }
    }
}
