﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SwapniApi.DTOs.Authentication
{

    /// <summary>
    /// Модель для трансортировки данных клиенту
    /// </summary>
    public class SignInDataDTO
    {
        [MinLength(6)]
        [MaxLength(60)]
        [Required]
        public string Password { get; set; }
        
        [EmailAddress]
        [Required]
        public string Email { get; set; }
    }
}
