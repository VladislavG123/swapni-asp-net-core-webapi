﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.DTOs.User
{
    public class ResetPasswordDTO
    {
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
    }
}
