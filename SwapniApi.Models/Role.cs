﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Models
{
    public static class Role
    {
        public const string User  = "user";
        public const string Administrator = "admin";
        public const string Developer = "dev";
    }
}
