﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace SwapniApi.Models
{
    public class Product
    {
        public Product()
        { }

        public Guid Id { get; set; } = Guid.NewGuid();
        public string Title { get; set; }
        public string Description { get; set; }
        public int Coins { get; set; }

        public DateTime CreationDate { get; set; } = DateTime.Now;
        public DateTime LastUpdateDate { get; set; }
        public DateTime DelitedDate { get; set; }

        public virtual Guid OwnerId { get; set; }


        public Product GetClone()
        {
            return this.MemberwiseClone() as Product;
        }
    }
}
