﻿using System;

namespace SwapniApi.Models
{
    public class Photo
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Url { get; set; }
    }
}
