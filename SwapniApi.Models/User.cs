﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Models
{
    public class User
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string Role { get; set; } = Models.Role.User;
    }
}
