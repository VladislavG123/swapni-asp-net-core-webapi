﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SwapniApi.Models
{
    public class ProductsPhoto
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public virtual Guid PhotoId { get; set; }
        public virtual Guid ProductId { get; set; }
    }
}
